from rest_framework import generics
from rest_framework.authentication import BasicAuthentication
from permissions import IsAuthenticatedOrCreate
from django.contrib.auth.models import User
from serializers import SignUpSerializer, LoginSerializer

class SignUp(generics.CreateAPIView):
    '''
    Signs up a new user
    '''
    queryset = User.objects.all()
    serializer_class = SignUpSerializer
    permission_classes = (IsAuthenticatedOrCreate,)

class Login(generics.ListAPIView):
    '''
    Returns the client id and client secret of an user when logging in
    '''
    queryset = User.objects.all()
    serializer_class = LoginSerializer
    authentication_classes = (BasicAuthentication,)

    def get_queryset(self):
        return [self.request.user]

