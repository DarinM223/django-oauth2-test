from django.db import models
from django.contrib.auth.models import User

class TodoList(models.Model):
    name = models.CharField(max_length = 100)
    user = models.ForeignKey(User)

class Todo(models.Model):
    description = models.CharField(max_length = 100)
    deadline = models.DateTimeField(blank = True)
    todolist = models.ForeignKey(TodoList)
