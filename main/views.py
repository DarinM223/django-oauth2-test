from rest_framework import generics, permissions
from models import TodoList
from oauth2_test.serializers import TodoListSerializer

# Create your views here.

class GetTodolists(generics.ListAPIView):
    '''
    View that gets all todolists that a user owns
    '''
    serializer_class = TodoListSerializer
    permission_classes = (permissions.IsAuthenticated,)
    def get_queryset(self):
        user = self.request.user
        return TodoList.objects.filter(user=user)
